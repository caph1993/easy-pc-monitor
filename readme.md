!This tool lets you print a short and useful summary of your system information.

By default, it prints the date, hour, cpu usage, cpu temperature, memory usage, network usage, battery status and wifi quality.

Example on the desktop panel:
![](panel_screenshot.png)

You can also use your own format or see the info as a json.
