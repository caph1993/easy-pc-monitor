import re, json, time, argparse, os
from subprocess import run, PIPE, DEVNULL

parser = argparse.ArgumentParser()
parser.add_argument("--json", action='store_true', help='print data as json')
parser.add_argument("--custom", help='custom string format')
parser.add_argument("--gui", action='store_true', help='custom string format')
args = parser.parse_args()

HERE = os.path.dirname(os.path.abspath(__file__))
json_file = f'{HERE}/latest_info.json'


def short(n, min_unit=''):
    if n<0: return '¿?'
    if n==0: return '0'
    units = ['G', 'M', 'k', '']
    while units[-1]!=min_unit: n /= 1000 ; units.pop()
    while units and n >= 700: n /= 1000 ; units.pop()
    if n>=10 or units[-1]==min_unit:
        s = f'{n:.0f}'
    else:
        s = f'{n:.1f}'
        #if s.endswith('.0'): s = s[:-2]
    return s + units.pop()


def get_core_temp():
    try:
        p = run('sensors', stdout=PIPE)
    except FileNotFoundError:
        raise Exception('Please install the lm-sensors package')
    core_regex = re.compile(r'Core *\d+: *((?:[-+]?\d*\.\d+|\d+)°[CF])')
    core_temps = core_regex.findall(p.stdout.decode())
    celsius = []
    for s in core_temps:
        temp, units = s.split('°')
        temp = float(temp)
        if units=='F':
            temp = (temp-32)*5/9
        celsius.append(temp)
    return round(max(celsius))


def get_cpu_usage():
    '''
    returns cpu_usage of user processes and system processes
    io_wait, nice, and all other non-user stuff is included into system
    '''
    try:
        p = run(['mpstat', '-o', 'JSON'], stdout=PIPE, stderr=DEVNULL)
    except FileNotFoundError:
        raise Exception('Please install the sysstat package')
    info = json.loads(p.stdout.decode())
    info = info['sysstat']['hosts'][0]['statistics'][0]['cpu-load'][0]
    user = info.pop('usr')
    idle = info.pop('idle')
    total = 100-idle
    others = max(0, total-user)
    info = {
        'cpu': round(total),
        'cpu_user': round(user),
        'cpu_sys': round(others),
    }
    return info


def get_memory_usage():
    p = run(['free', '-b', '--si'], stdout=PIPE, stderr=DEVNULL)
    regex = re.compile(r'(Mem|Swap): *(\d+) * (\d+)')
    info = regex.findall(p.stdout.decode())
    info = {key.lower(): (int(used), int(total)) for key, total, used in info}
    info = {
        'ram': info['mem'][0],
        'ram_str': short(info['mem'][0], min_unit='M'),
        'swap': info['swap'][0],
        'swap_str': short(info['swap'][0], min_unit='M'),
        'ram_percent':round(info['mem'][0]/info['mem'][1]),
        'swap_percent':round(info['swap'][0]/info['swap'][1]),
    }
    return info


def get_battery_usage():
    p = run('upower -i $(upower -e | grep "/battery")', shell=True, stdout=PIPE, stderr=DEVNULL)
    out = p.stdout.decode()
    try: level = int(re.search(r'percentage: *(\d+)%', out).group(1))
    except: level = 100
    try: state = re.search(r'state: *(\w*)', out).group(1)
    except: state = 'charging'
    info = {
        'bat_level': round(level),
        'bat_charging': int(state=='charging'),
    }
    return info


def get_network_counters():
    commands = {
        'tx_cnt': ['awk', 'NR>2 {if ($1!="lo:") sum+=$10} END {print sum}', '/proc/net/dev'],
        'rx_cnt': ['awk', 'NR>2 {if ($1!="lo:") sum+=$2} END {print sum}', '/proc/net/dev'],
        'wifi_quality': ['awk', 'NR>2 { print $3*10/7}', '/proc/net/wireless'],
    }
    info = {}
    for key, cmd in commands.items():
        p = run(cmd, stdout=PIPE, stderr=DEVNULL, check=False)
        out = p.stdout.decode().strip().replace(',', '.')
        info[key] = round(float(out)) if out else 0
    return info

    
def get_cpu_counters():
    cmd = ['cat', '/proc/stat']
    p = run(cmd, stdout=PIPE, stderr=DEVNULL, check=False)
    out = p.stdout.decode()
    regex = r'cpu +(?P<user>\d+) +(?P<nice>\d+) +(?P<sys>\d+) +(?P<idle>\d+) +(?P<iowait>\d+) +(?P<hardirq>\d+) +(?P<softirq>\d+)'
    vals = re.search(regex, out).groupdict()
    vals = {k:int(v) for k,v in vals.items()}
    info = {
        'cpu_on_cnt': vals['user']+vals['sys'],
        'cpu_user_cnt': vals['user'],
        'cpu_sys_cnt': vals['sys'],
        'cpu_sum_cnt': vals['user']+vals['sys']+vals['idle'],
    }
    return info

def get_diff(json_file, info):
    prev = {}
    if os.path.isfile(json_file):
        with open(json_file) as f:
            prev = json.loads(f.read() or '{}')
    diff = {}
    for k in info:
        try: diff[k] = info[k]-prev[k]
        except: diff[k] = 0
    return diff


def get_info():
    info = {
        'core_temp': get_core_temp(),
        **get_memory_usage(),
        **get_battery_usage(),
        **get_network_counters(),
        **get_cpu_counters(),
    }
    info['bat_symbol'] = '🔌' if info['bat_charging'] else '🔋'
    t = time.ctime()
    info['clock'] = t[-14:-8].strip()
    info['date'] = t[:-14].strip()
    info['time'] = time.time()
    diff = get_diff(json_file, info)
    def div(a,b):
        return a/b if b else -1
    info['rx_kBs'] = div(diff['rx_cnt'], diff['time'])
    info['tx_kBs'] = div(diff['tx_cnt'], diff['time'])
    info['rx_str'] = short(info['rx_kBs'], min_unit='k')
    info['tx_str'] = short(info['tx_kBs'], min_unit='k')
    info['cpu'] = round(100*div(diff['cpu_on_cnt'], diff['cpu_sum_cnt']))
    with open(json_file, 'w') as f:
        f.write(json.dumps(info, indent=2))
    return info


if __name__=='__main__':
    default_fmt = (
        '{date:<10s} {clock:>5s} {tx_str:>4s}↑ {bat_symbol}{bat_level}%'
        '\n'
        '{cpu:3d}% {core_temp:2d}°C  {ram_str:>5s} {rx_str:>4s}↓ 📡{wifi_quality:3d}'
    )
    info = get_info()
    if args.custom!=None:
        fmt = args.custom
    else:
        fmt = default_fmt

    if args.json:
        print(json.dumps(info, indent=4))
    elif args.gui:
        gui = os.path.join(HERE, 'gui.py')
        run([sys.executable, gui])
    else:
        print(fmt.format(**info))
