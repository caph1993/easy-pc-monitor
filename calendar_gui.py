from flexx import flx
from datetime import date, datetime
from calendar import Calendar
from holidays import CountryHoliday
import asyncio

class MyWidget(flx.Widget):

    def init(self):
        global eval
        super().init()
        self.sleep = eval("(t)=>new Promise(r=>setTimeout(r, t))")
        self.document = eval("document")
        self.newDate = eval("(...args)=>new Date(...args)")
        self.JSON = eval("JSON")
        self._newCircularReplacer = eval("""() => {
          //https://stackoverflow.com/a/53731154/3671939
          const seen = new WeakSet();
          return (key, value) => {
            if (typeof value === "object" && value !== null) {
              if (seen.has(value)) return;
              seen.add(value);
            }
            return value;
          };
        };""")

    def repr(self, obj, indent=None):
        indent = indent and ' '*indent
        handler = self._newCircularReplacer()
        return self.JSON.stringify(obj, handler, indent);


class MyApp_JS(MyWidget):
    CSS = '''
    .equal_width{
        flex-basis: 0;
        text-align: center;
    }
    .holiday{
        background: #f4f4f4;
    }
    .other-month{
        font-style: italic;
        color: #999;
    }
    .today{
        font-weight: bold;
        text-opacity: 1.0;
        //background: #ddd;
        //border-color: #ccc;
    }
    .text-center{
        text-align: center;
    }
    .large-clock{
        font-size: 3em;
    }
    .calendar-month{
        min-width:8em;
        width:8em;
    }
    '''
    ready = flx.BoolProp(False)

    def init(self):
        super().init()
        with flx.VBox():
            with flx.HBox():
                with flx.VBox():
                    self.wdate = flx.Label(text='',
                        css_class='text-center')
                    self.wclock = flx.Label(
                        text='', css_class='large-clock text-center')
            with flx.HBox():
                with flx.VBox() as self.wcalendar:
                    with flx.HBox() as self.wcalendar.header:
                        pass
                    with flx.VBox() as self.wcalendar.body:
                        pass
            self.clocks = ['America/Bogota', 'Europe/Paris',]
            with flx.HBox():
                with flx.VBox() as self.wclocks:
                    pass
        #self._init_focus()
        self._init_ready()
        return

    async def _init_focus(self):
        while self.document.hasFocus()==False:
            await self.sleep(50)
        #self.wprompt.set_focus()
        return

    async def _init_ready(self):
        #while not self.month or not self.rows:
        #    await self.sleep(50)
        self._mutate_ready(True)
        self._clocks()
        return

    async def _clocks(self):
        fmt_hour = {
            'hour12': False,
            'hour': '2-digit',
            'minute': '2-digit',
            'second': '2-digit',
        }
        fmt_date = {
            'weekday': 'long',
            #'year': 'numeric',
            'month': 'long',
            'day': 'numeric',
        }
        fmt_more = {
            'month': 'long',
            'day': 'numeric',
            'hour12': False,
            'hour': '2-digit',
            'minute': '2-digit',
        }

        with self.wclocks:
            for tz in self.clocks:
                flx.Label(text='-')
        while 1:
            now = self.newDate()
            self.wdate.set_text(now.toLocaleString('en-GB', fmt_date))
            self.wclock.set_text(now.toLocaleString('en-GB', fmt_hour))
            for tz, w in zip(self.clocks, self.wclocks.children):
                options = fmt_more.copy()
                options['timeZone'] = tz
                date = now.toLocaleString('en-GB', options)
                w.set_text(f'{date} {tz}')
            await self.sleep(500)
        return

    @flx.action
    def set_calendar(self, year, month, dates):
        months = ['January', 'February', 'March',
            'April', 'May', 'June', 'July', 'August',
            'September', 'October', 'November',
            'December']
        month_name = months[month-1]
        for x in self.wcalendar.header.children:
            x.dispose()
        for x in self.wcalendar.body.children:
            x.dispose()
        with self.wcalendar.header:
            flx.Button(text='Prev')
            flx.Label(text='', flex=1)
            flx.Label(text=f'{month_name}', css_class='calendar-month')
            flx.Button(text=f'-')
            flx.Label(text=f'{year}')
            flx.Button(text=f'+')
            flx.Label(text='', flex=1)
            flx.Button(text='Next')
        with self.wcalendar.body:
            with flx.HBox():
                for wd in 'Mo Tu We Th Fr Sa Su'.split():
                    flx.Label(text=wd, flex=1, css_class='equal_width')
            while len(dates)<6:
                dates.append([])
            for row in dates:
                with flx.HBox():
                    for date in row:
                        e = flx.Label(
                            text=f'{date.day}',
                            flex=1,)
                        css_class = 'equal_width'
                        if date.is_today:
                            css_class+=' today'
                        if date.is_holiday or date.weekday>=5:
                            css_class+=' holiday'
                        if date.month != month:
                            css_class+=' other-month'
                        e.set_css_class(css_class)
                    if not row:
                        flx.Label()
        return

    @flx.reaction('wcalendar.header.children*.pointer_click')
    def _on_click(self, *events):
        e = events[-1]
        v = {'+':'+year', '-':'-year', 'Prev':'+month',
            'Next':'-month'}.get(e.source.text, None)
        if v: self.emit_calendar(v)
        return

    @flx.emitter
    def emit_calendar(self, event):
        return dict(value=event)


class MyApp(flx.PyComponent):

    def init(self):
        self.ready = False
        self.js = MyApp_JS()
        self.holidays = CountryHoliday('CO')
        asyncio.ensure_future(self._post_init())
        today = datetime.now()
        self.month = today.month
        self.year = today.year
        return

    async def _post_init(self):
        while not self.js.ready:
            await asyncio.sleep(50e-3)
        holidays = CountryHoliday('Colombia')
        self.set_month(self.year, self.month)
        return

    def set_month(self, year, month):
        self.month = month
        self.year = year
        dates = Calendar().monthdatescalendar(year, month)
        dates = [[self.date2Dict(d) for d in l] for l in dates] 
        self.js.set_calendar(year, month, dates)
        return

    def date2Dict(self, date):
        today = datetime.now()
        is_today = (
            today.day==date.day and
            today.month==date.month and
            today.year==date.year)
        d = flx.Dict(
            year=date.year, month=date.month, day=date.day,
            weekday=date.weekday(),
            is_holiday=date in self.holidays,
            is_today=is_today,)
        return d

    @flx.reaction('js.emit_calendar')
    def _on_click_calendar(self, *events):
        e = events[-1]
        year = self.year
        month = self.month
        n = 1 if e.value.startswith('+') else -1
        if e.value.endswith('year'): year+=n
        else: month+=n
        if month==13: month=1 ; year+=1
        if month==0: month=12 ; year-=1
        self.set_month(year, month)

def main():
    title = 'Custom Calendar'
    icon = None
    app = flx.App(MyApp)
    UI = app.launch('app', title=title, icon=icon)
    flx.run()
    return


def freeze(elem, indent=1, max_depth=6, max_children=15):
    if max_depth==None: max_depth=float('inf')
    if max_children==None: max_children=float('inf')
    cache = []
    
    def _rep(d, elem):
        if isinstance(elem, list): oc = '[]'
        elif isinstance(elem, dict): oc = '{}'
        else: oc=None
        if oc=='[]' or oc=='{}':
            if any([elem is e for e in cache]):
                return '(circular reference)'
            cache.append(elem)
            if len(elem)==0:
                return oc                
            ans = []
            n = min(len(elem), max_children)
            if d>max_depth:
                ans.append(f'...(depth>{max_depth})...')
            else:
                if oc=='[]': keys = [i for i in range(n)]
                else: keys = [e for e in elem][:n]
                for key in keys:
                    value = _rep(d+1, elem[key])
                    ans.append(f'{key}: {value}')
                if len(elem) > n:
                    ans.append(f'...({len(elem)-n} more children)...')
            
            ans = oc[0] + ', '.join(ans) + oc[1]
        elif isinstance(elem, str):
            ans = f'"{elem}"'
        else:
            ans = f'{elem}'
        return ans
    return _rep(0, elem)

if __name__=='__main__':
    main()
