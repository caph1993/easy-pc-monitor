import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk, Gdk
from gi.repository import AppIndicator3 as Appindicator
from gi.repository import Notify
import signal, threading, time, os, sys, subprocess
from monitor import get_info

# Docs: https://lazka.github.io/pgi-docs/AppIndicator3-0.1/classes/Indicator.html
# The docs do not provide any styling method
# The librariy does not allow to remove the icon

HERE = os.path.abspath(os.path.dirname(__file__))+'/'
APPINDICATOR_ID = 'caph-monitor'
appQuit = False


def main():
    set_style()
    indicator = Appindicator.Indicator.new_with_path(
        id=APPINDICATOR_ID,
        icon_name=HERE+'jockey.png',
        category=Appindicator.IndicatorCategory.SYSTEM_SERVICES,
        icon_theme_path=os.path.abspath(os.path.dirname(__file__)))
    indicator.set_status(Appindicator.IndicatorStatus.ACTIVE)
    indicator.set_title(APPINDICATOR_ID)
    indicator.set_menu(build_menu())
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    threading.Thread(target=thread_monitor, args=[indicator], daemon=True).start()
    thread_gtk()
    return


def set_style():
    # Styling does not work currently for appindicator, only for Gtk windows
    css = b'* { background-color: #f00; font-size: 9px; }'
    css_provider = Gtk.CssProvider()
    css_provider.load_from_data(css)
    Gtk.StyleContext().add_provider_for_screen(
        Gdk.Screen.get_default(),
        css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    return


def thread_gtk():
    Notify.init(APPINDICATOR_ID)
    return Gtk.main()


def thread_monitor(indicator):
    fmt = (
        '{tx_str:>4s}↑{rx_str:>4s}↓{wifi_quality:3d}↕'
        '{cpu:3d}%{core_temp:3d}°C {ram_str:>4s}'
    )
    longest_case = fmt.format(
        cpu=100, core_temp=100, ram_str='1.7G',
        tx_str='1.7k', rx_str='1.7k', wifi_quality=100)
    while not appQuit:
        label = fmt.format(**get_info())
        indicator.set_label(label, longest_case)
        time.sleep(1)
    return


def build_menu():
    menu = Gtk.Menu()
    options = {
        'Quit': quit,
        'Calendar': openCalendar,
    }
    for k in options:
        item = Gtk.MenuItem(label=k)
        item.connect('activate', options[k])
        menu.append(item)
    menu.show_all()
    return menu


def quit(source):
    global appQuit
    Gtk.main_quit()
    appQuit = True


def notifyErrors(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            Notify.Notification.new(f'Error ({sys.executable})', f'{e}', None).show()
    return wrapper


@notifyErrors
def openCalendar(source):
    subprocess.run([sys.executable, HERE+'calendar_gui.py'])


if __name__ == "__main__":
    main()